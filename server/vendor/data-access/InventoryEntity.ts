/*************  © FocusResearch Labs 2020 *********************
Document Table: product_inventories 
Function : Store the product generated form the excel into the
Table and Retrieve it using index functionality.
***************************************************************/
import {productDetails} from '../models/Inventory';
import APIError from '../helpers/APIError';
import {DBEntity} from './DBEntity';  
var ObjectId = require('mongodb').ObjectID;

export class InventoryEntity extends DBEntity{

    constructor(){
      super(); 
    }    
 
    async findAll(findProduct) { 
    const result  = productDetails.find({$and:[{vendorId:findProduct.vendorId},{$or:[{description:findProduct.description},{name:findProduct.name},{subcategory:findProduct.subcategory},{vendorcategory:findProduct.vendorcategory},{vendorId:findProduct.vendorId}]}]},{item:0,keyword:0,barcode:0,createdDate:0,updatedDate:0,isDeleted:0,deletedAt:0})
    return result;
    } 

    async findById({ id: _id }) {
      const result = await super.getDB().collection('product_inventories').find({ _id });
      const found = await result.toArray();
      if (found.length === 0) {
        return null;
      }
      const { _id: id, ...info } = found[0];
      return { id, ...info };
    }

    async insert(productRecord) {
  let productRecords = new productDetails({
         vendorId: productRecord.body.vendorId,
         name:productRecord.body.name,
         description:productRecord.body.description,
         keywords:productRecord.body.keywords,
         barcode:productRecord.body.barcode,
         category:productRecord.body.category,
         subcategory:productRecord.body.subcategory,
         aliasName:productRecord.body.aliasName,
         item:productRecord.body.item,
         image: process.env.IMAGEURl + productRecord.file.path
        });

       var productRecordSaved = await productRecords.save();
          return productRecordSaved;
    
    } 

        async insertExcel(productRecord) {
          for(var i=0;i<=productRecord.length;i++){
            if(productRecord[i] != undefined){
        var lowerCaseConvertor = productRecord[i].vendorcategory +'-'+ productRecord[i].subcategory +'-'+productRecord[i].productcategory +'-' + productRecord[i].productsubcategory +'-'+productRecord[i].vendorId +'/'+productRecord[i].image;
        var folderPath = lowerCaseConvertor.toLowerCase();
        const productRecords = new productDetails({ 
         vendorId: productRecord[i].vendorId,
         name:productRecord[i].name,
         description:productRecord[i].description,
         keywords:productRecord[i].keywords,
         barcode:productRecord[i].barcode,
         vendorcategory:productRecord[i].vendorcategory, 
         subcategory:productRecord[i].subcategory, 
         productcategory:productRecord[i].productcategory,
         productsubcategory:productRecord[i].productsubcategory,
         aliasName:productRecord[i].aliasName,
         item:productRecord[i].item,
         image: process.env.IMAGEURl + folderPath
        });

      
        var productRecordSaved = await productRecords.save();

            }


          }
        return productRecordSaved;    
    }
 
    async update (updateProductRecord) {
 
var id = ObjectId(updateProductRecord.body.id);
    
var productItem = {
         vendorId: updateProductRecord.body.vendorId,
         subscriptionGroup:updateProductRecord.body.subscriptionGroup,
         name:updateProductRecord.body.name,
         description:updateProductRecord.body.description,
         keywords:updateProductRecord.body.keywords,
         category:updateProductRecord.body.category,
         subcategory:updateProductRecord.body.subcategory,
         allowDiscount:updateProductRecord.body.allowDiscount,
         discountPercent:updateProductRecord.body.discountPercent,
         discountValue:updateProductRecord.body.discountValue,
         aliasName:updateProductRecord.body.aliasName,
         renewal:updateProductRecord.body.renewal,
         adsImage: process.env.IMAGEURl + updateProductRecord.files.adsimage[0].path,
         productImage: process.env.IMAGEURl + updateProductRecord.files.productimage[0].path
         
        };


      const db = await super.getDB();
      const result = await db.collection('product_inventory')
        .updateOne({ _id :id  }, { $set:  productItem });
      return result.modifiedCount > 0 ? "Modified Successfully" : null;
    }


   async updateImage (updateProductRecord) {

var id = ObjectId(updateProductRecord.body.id);
    
var productItem = {
                 
        productImage: process.env.IMAGEURl + updateProductRecord.file.path
         
        };


      const db = await super.getDB();
      const result = await db.collection('product_inventory')
        .updateOne({ _id :id  }, { $set:  productItem });
      return result.modifiedCount > 0 ? "Modified Successfully" : null;
    }


   async updatesingleproduct (updateProductRecord) {

var id = ObjectId(updateProductRecord.params.id);
    
var productItem = {
         name:updateProductRecord.body.name,
         description:updateProductRecord.body.description,
         category:updateProductRecord.body.category,
         subcategory:updateProductRecord.body.subcategory,
         allowDiscount:updateProductRecord.body.allowDiscount,
         discountPercent:updateProductRecord.body.discountPercent,
         discountValue:updateProductRecord.body.discountValue,
         aliasName:updateProductRecord.body.aliasName,
         renewal:updateProductRecord.body.renewal,
         delivery:updateProductRecord.body.delivery,
         item:updateProductRecord.body.item.unit 
        };


      const db = await super.getDB();
      const result = await db.collection('product_inventory')
        .updateOne({ _id :id  }, { $set:  productItem });
      return result.modifiedCount > 0 ? "Modified Successfully" : null;
    }

 async remove(id) {
  var id = ObjectId(id)
    const db = await super.getDB();
    const result = await db.collection('product_inventory').deleteOne({ _id : id })
    return result.deletedCount  > 0 ? "deleted Successfully" : null;
  }


  }


