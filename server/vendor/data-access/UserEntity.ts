/*************  © FocusResearch Labs 2020 *********************
Document Table: users 
Function : store all the registered vendor
***************************************************************/
import APIError from '../helpers/APIError';
import {DBEntity} from './DBEntity'; 
import {UserEntity}  from '../models/Users';
import {Book} from '../models/User'
const Transaction = require("mongoose-transactions");



export class UsersEntity extends DBEntity{
 
    constructor(){
      super();
    }
  
    async findAll() {
      const result = await super.getDB().collection('users').find();
      return (await result.toArray()).map(({ _id: id, ...found }) => ({
        id,
        ...found
      }));
    } 

      async findUser(memberspayload) {
       const result  = UserEntity.find({ "authInfo.email": memberspayload.body.email })
       return result
    } 
    
    async findById({ id: _id }) {
      const result = await super.getDB().collection('users').find({ _id });
      const found = await result.toArray();                                                  
      if (found.length === 0) {
        return null;
      }
      const { _id: id, ...info } = found[0];
      return { id, ...info };
    }

    async insert(vendorRecord) { 
  let vendorRecords:any = new UserEntity({
    vendorId:vendorRecord.vendorId,
          name:vendorRecord.name,
          phone:vendorRecord.phone,
          isdcode:vendorRecord.isdcode,  
          fname:vendorRecord.fname,
          lname:vendorRecord.lname,
          status:vendorRecord.status,
          aliasName:vendorRecord.aliasName,
          role:vendorRecord.role,
          image:vendorRecord.image,
          authInfo:vendorRecord.authInfo
  });
       var vendorRecordSaved = await vendorRecords.save();
          return vendorRecordSaved;
    
    }
  
async insertTransction(vendorRecord:any,transaction:any) { 

const vendorObject =  new UserEntity({
          vendorId:vendorRecord.vendorId,
                name:vendorRecord.name,
                phone:vendorRecord.phone,
                isdcode:vendorRecord.isdcode,  
                fname:vendorRecord.fname,
                lname:vendorRecord.lname,
                status:vendorRecord.status,
                aliasName:vendorRecord.aliasName,
                role:vendorRecord.role,
                image:vendorRecord.image,
                authInfo:vendorRecord.authInfo
        });

const bookObject = new Book({
      title:vendorRecord.title,
      author:vendorRecord.author
     })

    const jonathanId = transaction.insert("Book",bookObject);
    transaction.insert("users",vendorObject); 
    const final = await transaction.run();
  
}   

    async update ({ id: _id, ...hmnInfo }) {
      const db = await this.getDB();
      const result = await db
        .collection('users')
        .updateOne({ _id }, { $set: { ...hmnInfo } });
      return result.modifiedCount > 0 ? { id: _id, ...hmnInfo } : null;
    }
    async findMember ({ id: _id, ...hmnInfo }) {
      const db = await this.getDB();
      const result = await db
        .collection('users')
        .updateOne({ _id }, { $set: { ...hmnInfo } });
      return result.modifiedCount > 0 ? { id: _id, ...hmnInfo } : null;
    }

  }

