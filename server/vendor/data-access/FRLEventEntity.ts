/*************  © FocusResearch Labs 2020 *********************
Document Table: frlglobalevents 
Function : Store system/user events, context and the corresponding
payload.
***************************************************************/
import  FRLGlobalEvent  from '../models/FRLGlobalEvent';
import  {DBEntity} from './DBEntity';
import APIError from '../helpers/APIError';
var ObjectId = require('mongodb').ObjectId;

export class FRLEventEntity extends DBEntity{

    constructor(){ 
      super();
    }

    async findAll() {
      const result = await super.getDB().collection('frlglobalevents').find({});
      return result;
    }
 
    async findById(id) {
      const objId = new ObjectId(id);
      const result = await super.getDB().collection('frlglobalevents').find({"_id":objId});
      const found = await result.toArray();
      if (found.length === 0) {
        return null;
      }
      return found[0];
    }

    async insert(frlEventData, _session) {

        const opts = { _session };
        let frlEventRecord = new FRLGlobalEvent({
          eventCode: frlEventData.code,
          eventName: frlEventData.name,
          dateTime: frlEventData.dateTime,
          session:  frlEventData.session,
          role: frlEventData.role
        });
        //pass mongoose session to be included into the transaction context
       // frlEventRecord = await frlEventRecord.saveAsync(opts);
        return frlEventRecord;
    }

    async update (frlEventData, _session) {
      const opts = { _session };
      const objId = new ObjectId(frlEventData._id);
      const db = await this.getDB();
      const result = await db
        .collection('frlglobalevents')
        .updateOne({ "_id": objId }, { $set: { ...frlEventData } });
      return result.modifiedCount > 0 ? result : null;
    }

    async delete (frlEventData) {
      const objId = new ObjectId(frlEventData._id);
      const result = await this.getDB().collection('frlglobalevents').deleteOne({"_id":objId});
      return result.deletedCount;
    }

  }

