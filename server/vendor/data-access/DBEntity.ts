/*************  © FocusResearch Labs 2020 *********************
Function : DB Inital Setup For Vendor
***************************************************************/
import mongoose from 'mongoose';
import config from '../config/env/index';


export class DBEntity { 

    constructor(){ 
         console.log("DBEntity initialized");
         this.setupDb();
        }
        private setupDb(): void {
            mongoose.connect(config.default.vendordb,{ useNewUrlParser: true});
            var db = mongoose.connection;
            db.on("error", console.error.bind(console, "MongoDB Connection error"));
            if (process.env.DEBUG === '*') {
                            mongoose.set("debug", (collectionName, method, query, doc) => {
                                console.log(`${collectionName}.${method}`, JSON.stringify(query), doc);
                            });
            }
          }

    getDB() {
        return mongoose.connection.db;
    }

   
}


























