/*************  © FocusResearch Labs 2020 *********************
Function : vendor registerstion interface factory
***************************************************************/
import {ServiceFactory} from '../ServiceFactory';
import membershipService from '../../service';


const service = new ServiceFactory(); 

export class MemebershipFactory{ 

        constructor(){
            
        }

        findVendorCtrl(){
            return  async function (httpRequest) {
                return service.invokeService(httpRequest.body, membershipService.findMember);
            }
        }
         registerVendorCtrl(){
            return  async function (httpRequest) {
                return service.invokeService(httpRequest.body, membershipService.registerVendor);
            }
        }
}

