
/*************  © FocusResearch Labs 2020 *********************
Function : Register Vendor Interface controller component.
***************************************************************/
  import {MemebershipFactory} from './MembershipFactory';
  import notfound from '../notfound'; 

  const memberFactory = new MemebershipFactory();

  const findMember = memberFactory.findVendorCtrl();
  
  const registerVendor = memberFactory.registerVendorCtrl();



  const membershipController = Object.freeze({
    findMember,
    notfound,
    registerVendor
  });


  export default membershipController;
  export { findMember, notfound, registerVendor}