
/*************  © FocusResearch Labs 2020 *********************
Function : Produt From Excel Interface controller component.
***************************************************************/
  import {InventoryFactory} from './InventoryFactory';
 
 
 
  const inventoryDetails = new InventoryFactory(); 

  const inventory = inventoryDetails.inventoryCtrl(); 

  const viewproducts = inventoryDetails.viewProductsCtrl(); 

  const inventoryexcel = inventoryDetails.inventoryExcelCtrl(); 


  

 
 
  const inventoryController = Object.freeze({
    inventory,
    viewproducts,
    inventoryexcel
  });
  
  export default inventoryController;
  export { inventory, viewproducts, inventoryexcel }