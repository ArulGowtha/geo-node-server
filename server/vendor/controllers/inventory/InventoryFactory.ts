/*************  © FocusResearch Labs 2020 *********************
Function : Product form excel processing occurs here
***************************************************************/
// import {InventoryServiceFactory} from './InventoryServiceFactory';
import {ServiceFactory} from '../ServiceFactory';
import inventoryService from '../../service/inventory';


const inventoryservice = new ServiceFactory();  

export class InventoryFactory{ 

        constructor(){
            
        }

        inventoryCtrl(){
            return  async function (httpRequest) {
                return inventoryservice.invokeService(httpRequest, inventoryService.inventory);
            }
        }

        viewProductsCtrl(){
            return  async function (httpRequest) {
                return inventoryservice.invokeService(httpRequest, inventoryService.viewproducts);
            }
        }

        inventoryExcelCtrl(){
            return  async function (httpRequest) {
                return inventoryservice.invokeService(httpRequest, inventoryService.inventoryexcel);
            }
        }


}

