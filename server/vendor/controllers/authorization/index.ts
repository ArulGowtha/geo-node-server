/*************  © FocusResearch Labs 2020 *********************
Function : Login Interface controller component.
***************************************************************/
import {AuthFactory} from './AuthentificationFactory';



const LoginDetails = new AuthFactory();

const loginUser = LoginDetails.loginUserCtrl();

const authController = Object.freeze({
  loginUser
});

export default authController;
export {
  loginUser 
}