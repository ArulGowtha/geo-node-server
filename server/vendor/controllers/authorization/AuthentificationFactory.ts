/*************  © FocusResearch Labs 2020 *********************
Function : LOGIN functionality has been implemented
***************************************************************/
// import {AuthServiceFactory} from './AuthentificationServiceFactory';
import {ServiceFactory} from '../ServiceFactory';
import authService from '../../service/common';

const authservice = new ServiceFactory();

export class AuthFactory {

    constructor() { 

    }

    loginUserCtrl() {
        return async function (httpRequest) {
            return authservice.invokeService(httpRequest, authService.loginUser);
        }
    }

}