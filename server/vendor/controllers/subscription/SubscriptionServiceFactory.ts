/*************  © FocusResearch Labs 2020 *********************
Function : Service factory.
***************************************************************/
export class SubscriptionServiceFactory{

    constructor() {

    }

    async invokeService(httpRequest, functionName){
        const payload = httpRequest;
        const responsePayload = await functionName(payload);
        return responsePayload;
    }
}

