/*************  © FocusResearch Labs 2020 *********************
Function : Member service interface factory
***************************************************************/
import {SubscriptionServiceFactory} from './SubscriptionServiceFactory';

const subscriptionservice = new SubscriptionServiceFactory();  

export class SubscriptionFactory{ 

        constructor(){
            
        }

        subscriptionCtrl(functionName){
            return  async function (httpRequest) {
                return subscriptionservice.invokeService(httpRequest, functionName);
            }
        }

}

