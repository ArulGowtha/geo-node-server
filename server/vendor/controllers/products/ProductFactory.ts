/*************  © FocusResearch Labs 2020 *********************
Function : Member service interface factory
***************************************************************/
// import {ProductServiceFactory} from './ProductServiceFactory';
import {ServiceFactory} from '../ServiceFactory';
import productService from '../../service/product';


const productservice = new ServiceFactory();  

export class productFactory{ 

        constructor(){
            
        }

        addProductCtrl(){
            return  async function (httpRequest) {
                return productservice.invokeService(httpRequest, productService.addProduct);
            }
        }

        editProductCtrl(){
            return  async function (httpRequest) {
                return productservice.invokeService(httpRequest, productService.editProduct);
            }
        }

        uploadProductCtrl(){
            return  async function (httpRequest) {
                return productservice.invokeService(httpRequest, productService.uploadProductImage);
            }
        }
        editSingleProductCtrl(){
            return  async function (httpRequest) {
                return productservice.invokeService(httpRequest, productService.editSingleProduct);
            }
        }
        deleteProductCtrl(){
            return  async function (httpRequest) {
                return productservice.invokeService(httpRequest, productService.deleteProduct);
            }
        }

}

