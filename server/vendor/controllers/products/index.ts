
/*************  © FocusResearch Labs 2020 *********************
Function : Interface controller component.
***************************************************************/
  import { productFactory } from './ProductFactory';
 


  const productDetails = new productFactory(); 

  const addProduct = productDetails.addProductCtrl(); 
  const editProduct = productDetails.editProductCtrl();
  const imageUpload = productDetails.uploadProductCtrl();
  const editSingleProduct = productDetails.editSingleProductCtrl();
  const deleteProduct = productDetails.deleteProductCtrl();

 

  const productController = Object.freeze({
    addProduct,
    editProduct,
    imageUpload,
    editSingleProduct,
    deleteProduct
  });
  
  export default productController;
  export { addProduct ,editProduct, imageUpload, editSingleProduct, deleteProduct}