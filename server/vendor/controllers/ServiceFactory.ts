/*************  © FocusResearch Labs 2020 *********************
Function : Common Service factory.
***************************************************************/
const Transaction = require("mongoose-transactions");
import APIError from '../helpers/APIError';

/********* Sample Piece of Work ***********/ 

import  {ruleService} from '../rules/service/ruleServiceDecorator';
import {Book}  from '../models/User';

const preProcessor  =   (callback, args, name, type)=>{
    let result: any;
    let vendorRecords:any = new Book({
        "title":"Guru",
        "author":"Sample"
    });
     vendorRecords.save();
    result = callback();
    return result;
};
const postProcessor  =   (callback, args, name, type)=>{
    let result: any;
    console.log("post start");
    result = callback();
    console.log("Ended");
    return result;
};

//*******End oF Sample Pre And Post Processor *******/

export class ServiceFactory {

    constructor() {

    }
    
    //*sample Annotation*//
    @ruleService(preProcessor)
    @ruleService(postProcessor)
    //*sample Annotation*//

    async invokeService(payload, functionName) {
        const transaction = new Transaction();
        try {
            const responsePayload = await functionName(payload, transaction);
            return responsePayload;
        } catch (error) {
            const rollbackObj = await transaction.rollback().catch(console.error(error));
            transaction.clean();
            if(error.error != undefined){
                throw new APIError(`Error Occured Due to ${error.error}`);
            }else{
                throw new APIError(`Error Occured Due to ${error}`);
            }
          
        }

    }
   
   
 
}

