/*************  © FocusResearch Labs 2020 *********************
Index: The Vendor Index 
Function : It Runs on port 3110 VendorApi
***************************************************************/
import {app} from './config/express';
import winstonInstance from './config/winston';
import config from './config/env';





const debug = require('debug')('MGD-API:index');
// listen on port config.port
const gudPort = process.env.PORT || config.default.port;
app.listen(gudPort, () => {
  console.log('process env port', gudPort);
  winstonInstance.info(`Listening on port ${gudPort}....`)
  debug(`server started on port ${gudPort}`);  
});




export default app;
