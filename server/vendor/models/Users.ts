/*************  © FocusResearch Labs 2020 *********************
Function : users schema
***************************************************************/
import * as mongoose from "mongoose";
import {Document, model, Model, Schema} from 'mongoose';


import bcrypt from 'bcrypt';

  

var credentialsSchema = new mongoose.Schema({
email: {
        type: String,
        validate: {
         validator: function(v) {
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v);
      },
        message: props => `${props.value} is not a valid email Address!`
    },
    required: [true, 'Email Address is required'],
        sparse:true,
        unique : true
    }, 
password: {
    type:String,
    required:true
},
challengeQuestion: {
    type:String,
    required:true
},  
answer:{
    type:String,
    required:true
},
deviceId:{
 type:String,
 required:true

},
accessCodeVerified:{
type:Boolean,
 required:true
}   
});

export const UsersScheme = new mongoose.Schema({
  vendorId:{
    type:String,
    required:true
  },
    name: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        validate: {
         validator: function(v) {
        return /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(v);
      },
        message: props => `${props.value} is not a valid phone number!`
    },
    required: [true, 'Phone number is required']
      },
    isdcode: {
        type: Number,
        required: true
    },    
    status: {
        type: String,
        required: true
      
    },
    fname: {
        type: String,
        required: true
      
    },
    lname: {
        type: String,
        required: true
      
    },
    aliasName: {
        type: String,
        required: true
      
    },
     role: {
        type: String,
        required: true
      
    },
     image: {
        type: String,
        required: false
      
    },
authInfo:[credentialsSchema],
createdDate: { type: Date, default: new Date().toISOString() },
createdBy:{type: String,required: false},
updatedDate: { type: Date, default: new Date().toISOString() },
isDeleted: { type: Boolean, default: false },
deletedAt: { type: Date, default: null },
updatedBy:{type: String,required: false}           
}); 


/**
 * converts the string value of the password to some hashed value
 * - pre-save hooks
 * - validations
 * - virtuals
 */
// eslint-disable-next-line
UsersScheme.pre('save', function userSchemaPre(next) { 
  const user:any = this;
  if (user.isModified('authInfo[0].password') || user.isNew) {
  //   // eslint-disable-next-line
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        return next(err);
      }

      // eslint-disable-next-line
      bcrypt.hash(user.authInfo[0].password, salt, (hashErr, hash) => {
        //eslint-disable-line
        if (hashErr) {
          return next(hashErr);
        }

        user.authInfo[0].password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});

/**
 * comapare the stored hashed value of the password with the given value of the password
 * @param pw - password whose value has to be compare
 * @param cb - callback function
 */
UsersScheme.methods.comparePassword = function comparePassword(pw, cb) {
  const that = this;
  bcrypt.compare(pw, that.credentials[0].password, (err, isMatch) => {

    if (err) {
      return cb(err);
    }
          cb(null, isMatch);
  });
};


UsersScheme.methods.toAuthJSON = function() {
  return {
    _id: this._id,
    email: this.email,
    token: this.generateJWT(),
  };
};

interface IUser extends Document {
  vendorId: string;
  name: number;
  phone: string[];
  isdcode: any[];
  status:any;
  fname: string;
  lname: number;
  aliasName: string[];
  role: any[];
  image:any;
  authInfo: string;

}



const UserEntity: Model<IUser> = mongoose.model<IUser>('users', UsersScheme);
export {UserEntity} ;
