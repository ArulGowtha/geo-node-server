/*************  © FocusResearch Labs 2020 *********************
Function : product schema
***************************************************************/

import mongoose, { Schema } from 'mongoose';




const ProductSchema = new Schema({
 
vendorId: { type: Schema.Types.ObjectId, ref: 'users' },
subscriptionGroup: {type:String,required: true },
name: String,
description: {type:String,required: true },
keywords: {type: [String],required:true},
category: {type:String,required: true },
subcategory: {type:String,required: true }, 
allowDiscount:{type:Boolean,required: true},
discountPercent:{type:String},
discountValue:{type:String},
aliasName:{type:String},
renewal:{
    frequency: {type : String,required: true},
    day:{type: Number,required: true}
},
delivery:{
    frequency: {type: [String],required: true},
    calendarDay: {type: [String],required: true},
    calendarMonth: {type: [String],required: true},
    flexibleSchedule: {type: Boolean},
    start: {type: [String]}

},
item:{
    unit:{type: String,required: true},
    default:{
qty:{type: Number,required: true},
price:{type: String,required: true}

    },
    options:[{
        quantity:{type: String,required: true},
        price:{type: String,required: true}
    }]
},
adsImage:{type:String,required: true},
productImage:{type:String,required: true},
createdAt: { type: Date, default: new Date().toISOString() },
updatedAt: { type: Date, default: new Date().toISOString() },
isDeleted: { type: Boolean, default: false },
deletedAt: { type: Date, default: null },

})

ProductSchema.path('name').required(true);

var productEntity=mongoose.model('product', ProductSchema);
export default productEntity;
