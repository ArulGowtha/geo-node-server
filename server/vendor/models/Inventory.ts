/*************  © FocusResearch Labs 2020 *********************
Function : product Inventory schema
***************************************************************/

import mongoose, { Schema } from 'mongoose';


 

export const InventorySchema = new mongoose.Schema({
 
// vendorId: { type: Schema.Types.ObjectId, ref: 'users' },
vendorId: {type:String,required: false },
name: String,
description: {type:String,required: true },
keywords: {type: [String],required:true},
barcode:{type: String,required:false},
vendorcategory:{type: String,required:true},
subcategory: {type:String,required: true },
productcategory: {type:String,required: true },
productsubcategory:{type:String,required: true},
aliasName:{type:String},
item:{
    unit:{type: String,required: true},
    default:{
qty:{type: String,required: true},
price:{type: String,required: true}

    },
    options:[{
        quantity:{type: String,required: false},
        price:{type: String,required: false}
    }]
},
image:{type:String,required: true},
createdDate: { type: Date, default: new Date().toISOString() },
createdBy:{type: String,required: false},
updatedDate: { type: Date, default: new Date().toISOString() },
isDeleted: { type: Boolean, default: false },
deletedAt: { type: Date, default: null },
updatedBy:{type: String,required: false}
})

InventorySchema.path('name').required(true);

const productDetails = mongoose.model('product_inventory', InventorySchema);
export {productDetails }