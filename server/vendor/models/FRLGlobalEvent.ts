/*************  © FocusResearch Labs 2020 *********************
Function : frlglobalevents schema
***************************************************************/
import mongoose, { Schema,Document  } from 'mongoose';

export interface IFrlEvents extends Document {
    eventCode: string;
    eventName: string;
    dateTime: string;
    session:string;
    role:string;
  }
export const frlGBLEventSchema = new Schema({
    eventCode: {
        type: String,
        required: true
    },
    eventName: {
        type: String,
        required: true
    },    
    dateTime: {
        type: String,
        required: true,
        default: Date.now()
    },
    session: {
        domain : { type: String, required: true},
        deviceId: { type: String, required:true},
        latitude : { type: String, required:true},
        longitude : { type: String, required:true},
        user : { type: String, required:true},
        task : { type: String, required:false},
        referrer: { type: String, required:false},
        referToken: { type: String, required:false},
        ip: { type:String, required:false}
    },
	role: {
		    name:{ type: String, required: true},
		    data: { type: Object, required: true }
		}
});
mongoose.models = {};
// module.exports=mongoose.model('FRLGlobalEvent', frlGBLEventSchema);
const FRLGlobalEvent = mongoose.model<IFrlEvents>('FRLGlobalEvent', frlGBLEventSchema);
export default FRLGlobalEvent;
