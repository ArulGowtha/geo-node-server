/*************  © FocusResearch Labs 2020 *********************
Function : Staging config
***************************************************************/
export default {
  env: 'staging',
  jwtSecret: '0a6b944d-d2fb-46fc-a85e-0295c986cd9f',
  db: 'mongodb://hnadmin:hnadmin2780@localhost:27017',
  dbname: 'gud-stage',
  dbConnectOptions: {
    server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }
  },
  fcm: {
    serverKey: "AAAAc6eF5vs:APA91bFC2K5h1FaOqnGuRK_onpJW6qxMQM-EfVv0372vd7dyielx0eYt2tJEvn440HSC0fcXtxzGUxn1eX2EvsE73HVsHhIB-glCnwAK6MGvqZrX10OKMF1RxuhXj9XL69s0sghB_uhM"
  },
  // port: 3041,
  port: 4203,
  passportOptions: {
    session: false,
  }
};
