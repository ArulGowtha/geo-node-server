/*************  © FocusResearch Labs 2020 *********************
Function : Production config
***************************************************************/
export default {
  env: 'production',
  jwtSecret: '0a6b944d-d2fb-46fc-a85e-0295c986cd9f',
  db: 'mongodb://hnadmin:hnadmin2780@localhost:27017',
  dbname: 'gud-prod',
  dbConnectOptions: {
    server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }
  },
  fcm: {
    serverKey: "AAAAc6eF5vs:APA91bFC2K5h1FaOqnGuRK_onpJW6qxMQM-EfVv0372vd7dyielx0eYt2tJEvn440HSC0fcXtxzGUxn1eX2EvsE73HVsHhIB-glCnwAK6MGvqZrX10OKM$"
  },
  // port: 3041,
  port: 4202,
  passportOptions: {
    session: false,
  }
};

