/*************  © FocusResearch Labs 2020 *********************
Function : Test config
***************************************************************/
export default {
  env: 'test',
  jwtSecret: '0a6b944d-d2fb-46fc-a85e-0295c986cd9f',
  db: 'mongodb://localhost:27017/hndev',
  port: 4123,
  passportOptions: {
    session: false,
  }
};
