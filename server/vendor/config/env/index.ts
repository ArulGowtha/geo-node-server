import path from 'path';
require('dotenv').config();

console.log("env : ", process.env.NODE_ENV);
console.log("DEBUG : ", process.env.DEBUG);
console.log("PORT : ", process.env.PORT);

console.log('process env', process.env.NODE_ENV);

const env = process.env.NODE_ENV || 'development';
const config = require(`./${env}`); //eslint-disable-line

const defaults = {
  root: path.join(__dirname, '/..'),
};
export default Object.assign(defaults, config);
