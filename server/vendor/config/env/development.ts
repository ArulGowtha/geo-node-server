/*************  © FocusResearch Labs 2020 *********************
Function : Development config
***************************************************************/
export default {
  env: 'development',
  jwtSecret: '0a6b944d-d2fb-46fc-a85e-0295c986cd9f', 
  // db: 'mongodb://ridesharingapp:ridesharingapp@localhost/ridesharingapp',
  //db: 'mongodb://localhost/shuttle-dev-db-v1',
  vendordb: 'mongodb://localhost:27017/geo-vendor',
  memberdb: 'mongodb://localhost:27017/geo-member',
  agencydb: 'mongodb://localhost:27017/geo-agency', 
  courierdb:'mongodb://localhost:27017/geo-courier',
  dbConnectOptions: {
    server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }
  },
  fcm: {
    serverKey: "AAAAc6eF5vs:APA91bFC2K5h1FaOqnGuRK_onpJW6qxMQM-EfVv0372vd7dyielx0eYt2tJEvn440HSC0fcXtxzGUxn1eX2EvsE73HVsHhIB-glCnwAK6MGvqZrX10OKMF1RxuhXj9XL69s0sghB_uhM"
  },
  // port: 3041,
  port: 3110,
  passportOptions: {
    session: false,
  }
};
