// import Joi from 'joi';
// // import { USER_TYPE_MEMBER, USER_TYPE_VENDOR, USER_TYPE_ADMIN } from '../constants/user';

// export default {
//   // POST /api/users/register
//   createUser: {
//     body: {
//       email: Joi.string().required().label('Email is required'),
//       // Minimum eight characters, at least one letter, one digit and one special character
//       // password: Joi.string().regex(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)
//       // .required().label("Invalid Password: Password should contain minimum eight characters, at least one letter, one digit and one special character"),
//       password: Joi.string().regex(/^(?=.*?[a-zA-Z])(?=.*?[0-9]).{8,}$/)
//       .required().label("Invalid Password: Password should contain minimum eight characters, at least one letter, one digit"),
//       phoneNo: Joi.string().required().label('Phone No. is required')
//     },
//   },

//   createUserAdmin: {
//     body: {
//       email: Joi.string().required().label('Email is required'),
//       // Minimum eight characters, at least one letter, one digit and one special character
//       // password: Joi.string().regex(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)
//       // .required().label("Invalid Password: Password should contain minimum eight characters, at least one letter, one digit and one special character"),
//       // password: Joi.string().regex(/^(?=.*?[a-zA-Z])(?=.*?[0-9]).{8,}$/)
//       // .required().label("Invalid Password: Password should contain minimum eight characters, at least one letter, one digit"),
//       phoneNo: Joi.string().required().label('Phone No. is required')
//     },
//   },

//   // PUT /api/users/mobile-phone
//   updatePhoneNo: {
//     body: {
//       phoneNo: Joi.string().required().label("Phone no. is required"),
//       isdCode: Joi.string().required().label("ISD Code is required"),
//       countryCode: Joi.string().required().label("Country code is required")
//     },
//   },

//   // UPDATE /api/users
//   updateUser: {
//     body: {
//       fname: Joi.string().required().label("First name is required"),
//       lname: Joi.string().required().label("Last name is required"),
//       phoneNo: Joi.string().required().label("Phone no. is required"),
//     },
//   },


//   // POST /api/auth/login
//   login: {
//     body: {
//       email: Joi.string().required().label('Email is required'),
//       password: Joi.string().required().label('Password is required'),
//       userType: Joi.string().required().label('User type is required'),
//     },
//   },

//   // GET /api/admin/user
//   userList: {
//     query: {
//       limit: Joi.number()
//         .integer()
//         .min(1),
//       pageNo: Joi.number()
//         .integer()
//         .min(1),
//       userType: Joi.string().required(),
//     },
//   },

//   // Get /api/admin/approvePendingUsers
//   pending: {
//     query: {
//       userType: Joi.string().required(),
//     },
//   },
//   // PUT /api/admin/approveUser
//   approve: {
//     query: {
//       id: Joi.string()
//         .alphanum()
//         .required(),
//     },
//   },

//   reject: {
//     query: {
//       id: Joi.string()
//         .alphanum()
//         .required(),
//     },
//   },


//   updateUserByAdmin: {
//     body: {
//       _id: Joi.string()
//         .alphanum()
//         .required(),
//       userType: Joi.string()
//         .valid(USER_TYPE_VENDOR, USER_TYPE_MEMBER)
//         .required(),
//     },
//   },

//  // for admin/partner creating new user(driver/admin)
//   createNewAdminUser: {
//     body: {
//       userType: Joi.string()
//         .valid(USER_TYPE_MEMBER, USER_TYPE_ADMIN)
//         .required(),
//       email: Joi.string()
//         .email()
//         .required(),
//       phoneNo: Joi.string().required(),
//       locationId: Joi.string().required()
//     },
//   },

//   onlineOffline: {
//     body: {
//       adminId: Joi.string()
//         .alphanum()
//         .required(),
//       driverId: Joi.string()
//       .alphanum()
//       .required()
//     },
//   },

//   requestNewAccessCode: {
//     body: {
//       adminId: Joi.string()
//         .alphanum()
//         .required(),
//       userType: Joi.string()
//         .valid(USER_TYPE_VENDOR, USER_TYPE_ADMIN)
//         .required(),
//       phoneNo: Joi.string().required()
//     },
//   },

//   addReview: {
//     body: {
//       reviewerId: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
//       reviewToType: Joi.string().required()
//       // message: Joi.string().required()
//     },
//   },
//   // GET /api/admin/user

//   createAdmin: {
//     body: {
//       email: Joi.string().required().label('Email is required'),
//       phoneNo: Joi.string().required().label('Phone No. is required')
//     },
//   },

//   createFaq: {
//     body: {
//       question: Joi.string().required().label('Question is required'),
//       answer: Joi.string().required().label('Answer is required')
//     },
//   }

// };
