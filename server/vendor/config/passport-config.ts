/*************  © FocusResearch Labs 2020 *********************
Function : Passport configuartion
***************************************************************/
import passportJWT from 'passport-jwt';
import cfg from './env'; 
import {UserEntity} from '../models/Users';
import membershipService from '../service';

 
const { ExtractJwt } = passportJWT; 
const jwtStrategy = passportJWT.Strategy;

function passportConfiguration(passport) {

  
  const opts:any = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
  // opts.tokenQueryParameterName = ExtractJwt.fromUrlQueryParameter(auth_token);
  opts.secretOrKey = cfg.default.jwtSecret;
  passport.use(new jwtStrategy(opts, (jwtPayload, cb) => {
    // membershipService.findMember({_id: jwtPayload._id })
    // UserEntity.findOne({ _id: jwtPayload._id }) //eslint-disable-line
      // .then(user =>  cb(null, user))
        return cb(null, "user")

  }));
}

export default passportConfiguration;
