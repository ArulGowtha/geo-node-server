/*************  © FocusResearch Labs 2020 *********************
Function : Logging component.
***************************************************************/

const logger = (req, res, next) => {
    console.log(`${req.originalUrl}`);
    next();
}

module.exports=logger;