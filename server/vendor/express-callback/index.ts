/*************  © FocusResearch Labs 2020 *********************
Function : callback controller entry point that orhestrates the 
inbound requests with application functionality and handle errors.
***************************************************************/
import APIError from '../helpers/APIError';
import httpStatus from 'http-status'; 
var exceltojson = require("xlsx-to-json-lc"); 


export const makeCallback  = function(controller) {  
    return (req, res, next) => {
      const httpRequest = {
        body: req.body,
        query: req.query, 
        params: req.params,
        ip: req.ip, 
        method: req.method,
        files:req.files,
        file:req.file,
        path: req.path,
        headers: {
          'Content-Type': req.get('Content-Type'),
          Referer: req.get('referer'),
          'User-Agent': req.get('User-Agent')
        }
      }
      //Call the service api through service controller factory.      
      controller(httpRequest)
        .then(responseObj => {
            return res.send(responseObj);
        })
        .catch(err => {
          const apiErr = new APIError(`${err}`, httpStatus.INTERNAL_SERVER_ERROR);          
          next(apiErr);
        })
    }
  }  

  export const makeCallbackExcel = function(controller) { 
    return (req, res, next) => {
 const jsonList =[];
 exceltojson({
    input: "./productlist.xlsx",
    output: "null"
  }, function(err, result) {
    if(err) {
        res.json(err);
    } else {
    if(result.length != undefined){
       for(var i = 0;i<result.length;i++){
        if(result[i] != undefined){
       jsonList.push({name:result[i].name,
        description:result[i].description,
        keywords:result[i].keywords,
        barcode:result[i].barcode,
        vendorcategory:result[i].vendorcategory,
        subcategory:result[i].subcategory,
        productcategory:result[i].productcategory,
        productsubcategory:result[i].productsubcategory,
        aliasName:result[i].aliasName,
        vendorId:"123",
        image:result[i].image,
        item:{
          unit:result[i].unit,
          default:{ 
            qty:result[i].qty,
            price:result[i].price
          },
          options:[{
          quantity:result[i].optionalqty1,
          price:result[i].optionalqty1price
         },{
          quantity:result[i].optionalqty2,
          price:result[i].optionalqty2price
        },{
          quantity:result[i].optionalqty3,
          price:result[i].optionalqty3price
        },{
          quantity:result[i].optionalqty4,
          price:result[i].optionalqty4price
        }]
     }})}

       }
     }
    }
controller(jsonList)
        .then(responseObj => {
            return res.send(responseObj);
        })
        .catch(err => {
          const apiErr = new APIError(`${err}`, httpStatus.INTERNAL_SERVER_ERROR);          
          next(apiErr);
        })

});      
}
}  