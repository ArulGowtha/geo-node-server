
/*************  © FocusResearch Labs 2020 *********************
Function : users - Unit Test Stub
***************************************************************/
require("babel-register");
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai,  {expect}  from 'chai';
import app from '../../vendor';
import 'mocha';


chai.config.includeStack = true;

describe('## User(Rider) APIs', () => {
  let registerVendor = {
    "title":"hello",
    "author":"adasdasd",
    "vendorId" : "er3424",
    "name" : "Jason Zheng", 
    "phone":"9841528851",
    "isdcode":"011",
    "status":"active",
    "fname":"Jason",
    "lname":"zheng",
    "aliasName":"naveen",
    "role": "supervisor",
    "image":"/prd/profile/img001.jpg",
    "authInfo":{
      "email":"naveenasds@gmail.com",
      "password":"123",	
      "challengeQuestion":"Where do i live?",
      "jwtAccessToken":"AKKNKH231HSFV",
      "deviceId":"wqe31234234242",					
      "answer":"guindy",
      "accessCodeVerified":"true"
    }
};


  let jwtAccessToken = null;

  describe('# POST api/member/registerVendor', () => {
    it('should create a new user', (done) => {
      request(app)
        .post('/api/member/registerVendor')
        .send(registerVendor)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          // expect(res.body.data).to.have.all.keys('user', 'jwtAccessToken');
          // rider1 = res.body.data.user;
          // jwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });


});

