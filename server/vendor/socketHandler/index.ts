// /*************  © FocusResearch Labs 2020 *********************
// Function : Socket Handler entry point
// ***************************************************************/
// import dashboardHandler from './story/admin-socket';
// import SocketStore from '../service/util/socket-store';
// import userHandler from './story/user-handler';
// import { USER_TYPE_ADMIN } from '../constants/user';

// const socketHandler = (socket) => {
//   dashboardHandler(socket);
//   userHandler(socket);

//   socket.on('hello', (data) => {
//     console.log('listen to hello', data);
//     socket.emit('helloResponse', 'hello everyone');
//   });

//   socket.on('disconnect', () => {
//     console.log('disconnecting socket, userType, id', socket.userType, socket.userId);
//     let userId = `${socket.authToken}/${socket.userId}/`;
//     SocketStore.removeByUserId(userId, socket);
//   });
// };

// export default socketHandler;
