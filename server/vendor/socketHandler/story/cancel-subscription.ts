// /*************  © FocusResearch Labs 2020 *********************
// Function : Subscription Socket Handler
// ***************************************************************/
// import moment from 'moment';
// import subsSchema from '../../models/subs';
// import SocketStore from '../../service/socket-store';
// import UserSchema from '../../models/user.js';
// import paymentCtrl from '../../controllers/payment'; //eslint-disable-line

// function cancelsubsHandler(socket) {
//     socket.on('cancelSubscription', (subsObj, cb) => {
//         console.log('CALLED FST TIME');
//         const riderID = subsObj.riderId;
//         const driverID = subsObj.driverId;
//         const subsID = subsObj.subsId;
//         let currentDate = new Date().toISOString();
//         let pickUpTime;
//         subsSchema.findOneAsync({ _id: subsID })
//             .then(subsData => {
//                 if (subsData) {
//                     if (subsData.subsStatus == 'unclaimed') {
//                         cancelsubs(subsData);
//                     }
//                     else {
//                         pickUpTime = subsData.pickUpTime;
//                         applyCancellationRules(subsData, currentDate, pickUpTime)
//                     }
//                 }
//                 else {
//                     SocketStore.emitByUserId(riderID, `server error while finding subs`, {});
//                 }
//             })
//             .catch(error => {
//                 SocketStore.emitByUserId(riderID, `server error while charging cancellation fees ${error}`, {});
//             })
//     });

//     function applyCancellationRules(subsData, currentDate, pickUpTime) {
//         let timeDifference = moment(pickUpTime).diff(currentDate, 'milliseconds');
//         if (timeDifference < 3600000) {
//             chargeCancellationFees(subsData, '100');
//         }
//         else if (timeDifference < 28800000) {
//             subsData.subsAmt = subsData.subsAmt / 2
//             chargeCancellationFees(subsData, '50');
//         }
//         else {
//             cancelsubs(subsData)
//         }
//     }

//     function chargeCancellationFees(subsData, percentage) {
//         paymentCtrl.cardPayment(subsData).then(status => {
//             if (status != 'error') {
//                 subsSchema.findByIdAndUpdateAsync({ _id: subsData._id }, { $set: { paymentStatus: status, subsStatus: "cancelled" } })
//                     .then(updatedsubsObj => {
//                         SocketStore.emitByUserId(updatedsubsObj.riderID, `subs is successfully cancelled and ${percentage} % cancellation fees is charged`, updatedsubsObj);
//                     })
//                     .catch(error => {
//                         SocketStore.emitByUserId(updatedsubsObj.riderID, `server error while cancelling subs ${error}`, {});
//                     })
//             }
//             else {
//                 SocketStore.emitByUserId(subsData.riderID, `server error while charging cancellation fees`, {});
//             }
//         });
//     }

//     function cancelsubs(subsData) {
//         subsSchema.findByIdAndUpdateAsync({ _id: subsData._id }, { $set: { subsStatus: "cancelled" } })
//             .then(updatedsubsObj => {
//                 SocketStore.emitByUserId(updatedsubsObj.riderID, `subs is successfully cancelled`, updatedsubsObj);
//             })
//             .catch(error => {
//                 SocketStore.emitByUserId(updatedsubsObj.riderID, `server error while cancelling subs ${error}`, {});
//             })
//     }
// }

// export default cancelsubsHandler;