/*************  © FocusResearch Labs 2020 *********************
Function : APIError object
***************************************************************/
import httpStatus from 'http-status';
import ExtendableError = require('extendable-error-class');

/**
 * @extends Error
 */

class ExtendableError extends Error {
  status:any;
  isPublic:any;
  isOperational:any;

  
  constructor(message, status, isPublic) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    this.status = status;
    this.isPublic = isPublic;
    this.isOperational = true;
    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else { 
      this.stack = (new Error(message)).stack; 
    }
    // Error.captureStackTrace(this, this.name);
  }
}
/**
 * Class representing an API error.
 * @extends ExtendableError
 */
class APIError extends ExtendableError {
  /**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   */
  constructor(message, status = httpStatus.INTERNAL_SERVER_ERROR, isPublic = true) {
    super(message, status, isPublic);
  }
}

export default APIError;
