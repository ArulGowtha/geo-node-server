export class RuleError {
  errMessages :any
    constructor(){
        this.errMessages = []; 
    }

    toString(){
        var errorString = null;
        for (let i = 0; i < this.errMessages.length; i++) {
            if (i == 0) {
                errorString = this.errMessages[i].toString();
            } else {
                errorString = errorString.concat('|').concat(this.errMessages[i]);
            }
        }        
        return errorString;        
    }

    getMessages(){
        return this.errMessages;
    }

    pushMessage(message){
        this.errMessages.push(message);
    }


}
