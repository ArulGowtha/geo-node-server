import * as frlRlues from '.';
const rulesConfig = require('./rulesConfig.json'); 

export  function ruleService(wrapperMethod) {
    return (target, key, descriptor) => {
  
      if ( typeof(target) === 'function' ){
        let newTarget = async function (...arg) { 
          var self = this;
          return async function() {
            var methodCallback = async function(){return new target(arg)};
            return await wrapperMethod.call(self, methodCallback, arg, target.name, 'class')
          }()
        };
        return newTarget;
      } else {
        let orgMethod = descriptor.value;
        descriptor.value = async function (...arg) {
          var self = this;
          return async function() {
            var methodCallback = async function() { return await orgMethod.apply(self, arg) };
            return await wrapperMethod.call(self, methodCallback, arg, key, 'function')
          }()
        };
        return descriptor;
      }
    }
  }
export async function rulesProcessor (callback, args, name, type)  {
  
    let ruleServicedConfig = rulesConfig.service.find(o => o.name === name);
    let result: any;
    if(ruleServicedConfig && ruleServicedConfig.enable=="true"){

     
      if(ruleServicedConfig.runBefore=="true"){     
        const validation  = await frlRlues.fireAllRules(args);
        
      }
      result = callback();
      if(ruleServicedConfig.runAfter=="true"){
        console.log("Ended");
      }

    }else{
      result = callback();
    }
    
    return result;

};

