/*************  © FocusResearch Labs 2020 *********************
Function : User Registration rules
***************************************************************/
import RuleFact from './RuleFact'; 
const nools = require('nools'); 


    export const registrationRules = nools.flow('User Communication Validation Rules', (builder) => {
        // Match the pattern
        builder.rule('ValidateEmail', [RuleFact, 'rule', 'isDefined(rule.ruleData[0].authInfo.email)'], (facts, session) => {
            if(!isEmailAddress(facts.rule.ruleData[0].authInfo.email)){
                    facts.rule.ruleError.pushMessage("Invalid Email Address");
            }
        }); 

        builder.rule('ValidatePhone', [RuleFact, 'rule', 'isDefined(rule.ruleData[0].phone)'], (facts, session) => {
            if(!isPhoneNumber( facts.rule.ruleData[0].phone)){
                    facts.rule.ruleError.pushMessage("Invalid Phone");
            }
        });

    });

    function isEmailAddress(email){
        return /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/.test(email);
    }
 
    function isPhoneNumber(phone){
        return /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(phone);
    }
    function isDefined(val){
        return (typeof(val)!=="undefined")?true:false;
    }