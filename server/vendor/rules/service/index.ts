
/*************  © FocusResearch Labs 2020 *********************
Function : All Member Related rules are invoked from here
***************************************************************/

import APIError from '../../helpers/APIError';
import {RuleError} from '../../helpers/RuleError';
import RuleFact from './RuleFact';
import * as userRuleFlow from './userRules';
import httpStatus from 'http-status';



export const fireAllRules =  (ruleData:any) => {
    const ruleError = new RuleError();
    const mbr = new RuleFact (ruleData, ruleError);
    const session = userRuleFlow.registrationRules.getSession(mbr);
    return session.match().then(() => {
            if(ruleError.getMessages() != ""){
                throw new APIError(ruleError.getMessages());
            }
        session.dispose();
    });
}

