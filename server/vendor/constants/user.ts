/*************  © FocusResearch Labs 2020 *********************
Function : User Constants.
***************************************************************/
export const USER_TYPE_MEMBER = 'member';
// export const USER_TYPE_VENDOR = 'vendor';
export const USER_TYPE_ADMIN = 'admin';
export const USER_TYPE_SUPER_ADMIN = 'superAdmin';
export const USER_TYPE_ANONYMOUS = 'anonymous';