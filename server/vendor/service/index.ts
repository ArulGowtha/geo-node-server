/*************  © FocusResearch Labs 2020 *********************
Function : Service routes
***************************************************************/
import {registerMember, findMember, vendorModule} from './member/MemberService';

const usersEntityobj = new vendorModule();
const registerVendor = usersEntityobj.registerVendor;

const handleInterface = intRequestPayload => {
  // TODO: Make real third party api call
};
 
const membershipService = Object.freeze({
  findMember,
  registerMember,
  registerVendor
}); 

export default membershipService;
//export { findMember, registerMember }