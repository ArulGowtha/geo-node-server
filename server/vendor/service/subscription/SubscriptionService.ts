// /*************  © FocusResearch Labs 2020 *********************
// Function : Membership service
// ***************************************************************/
//   import SubscriptionEntity from '../../data-access/SubscriptionEntity';
//   import APIError from '../../helpers/APIError';
//   import httpStatus from 'http-status';

//   export const subscription = async productPayload => {
//     const returnObj = {
//       success: false,
//       message: '',
//       data: null,
//       };
  
//     try{
//       //Initialize the entity
//       const subscriptionEntity = new SubscriptionEntity();
//       //Insert into Entity
//       const subscriptionRecord = await subscriptionEntity.insert(productPayload);
//       returnObj.success = true;
//       returnObj.message = 'Product Added Successfully';
//       returnObj.data = subscriptionRecord;
//       return returnObj;
//     }catch(err){
//       throw new APIError(`Error while Adding the Product ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
//     }

//   };

 