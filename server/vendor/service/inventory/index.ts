/*************  © FocusResearch Labs 2020 *********************
Function : Service routes
***************************************************************/
import { inventory, viewproducts, inventoryexcel } from './InventoryService';

const handleInterface = intRequestPayload => {
  // TODO: Make real third party api call
};
 
const inventoryService = Object.freeze({
  inventory,
  viewproducts,
  inventoryexcel
}); 

export default inventoryService;
