/*************  © FocusResearch Labs 2020 *********************
Function : Inventory service
***************************************************************/
  import {InventoryEntity} from '../../data-access/InventoryEntity';
  import APIError from '../../helpers/APIError';
  import httpStatus from 'http-status';

  export const inventory = async productPayload => {
    const returnObj = {
      success: false, 
      message: '', 
      data: null,
      };
  
    try{
      //Initialize the entity
      const inventoryEntity = new InventoryEntity();
      //Insert into Entity
      const inventoryRecord = await inventoryEntity.insert(productPayload);
      returnObj.success = true;
      returnObj.message = 'Product Added Successfully';
      returnObj.data = inventoryRecord;
      return returnObj;
    }catch(err){
      throw new APIError(`Error while Adding the Product ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
    }

  };
  

    export const viewproducts = async productPayload => {
    const returnObj = {
      success: false, 
      message: '', 
      data: null,
      };
  
    try{
      //Initialize the entity
      const inventoryEntity = new InventoryEntity();
      //Insert into Entity
      const inventoryRecord = await inventoryEntity.findAll(productPayload.body);
      returnObj.success = true;
      returnObj.message = 'LIST OF PRODUCTS';
      returnObj.data = inventoryRecord;
      return returnObj;
    }catch(err){
      throw new APIError(`Error while Listing the Product ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
    }

  };

export const inventoryexcel = async productPayload => {
    const returnObj = {
      success: false, 
      message: '' 
      };
  
    try{
      //Initialize the entity
      const inventoryEntity = new InventoryEntity();
      //Insert into Entity
      const inventoryRecord = await inventoryEntity.insertExcel(productPayload);
      returnObj.success = true;
      returnObj.message = 'Product Added Successfully';
      return returnObj;
      
     
    }catch(err){
      throw new APIError(`Error while Adding the Product ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
    }

  };
 