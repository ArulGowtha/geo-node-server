/*************  © FocusResearch Labs 2020 *********************
Function : Membership service
***************************************************************/
  // import MemberEntity from '../../data-access/MemberEntity';
  import {UsersEntity} from '../../data-access/UserEntity'; 
  import APIError from '../../helpers/APIError';
  import  {ruleService,rulesProcessor} from '../../rules/service/ruleServiceDecorator';


  import httpStatus from 'http-status';

  export const registerMember = async memberPayload => {

    const returnObj = {
      success: false,
      message: '',
      data: null,
      }; 
  
    try{
      //Initialize the entity

      const memberEntity = new UsersEntity();
      //Insert into Entity
      const memberRecord = await memberEntity.insert(memberPayload);
      returnObj.success = true;
      returnObj.message = 'Member Registered successfully';
      returnObj.data = memberRecord;
      return returnObj;
    }catch(err){
      throw new APIError(`Error while Registering member ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
    }

  };
 
  export const findMember = memberPayload => {

      const returnObj = {
        success: false,
        message: '',
        data: null,
        };
  
      try{
        const memberEntity = new UsersEntity();
        //To be implemented
        const memberRecord =  memberEntity.findMember(memberPayload);
        returnObj.success = true;
        returnObj.message = 'Member Registered successfully';
        returnObj.data = memberRecord;
        return returnObj;
      }catch(ex){
        throw new APIError(`Error while Registering member ${ex}`, httpStatus.INTERNAL_SERVER_ERROR);
      }      
  };
  // export const registerVendor = async (memberPayload,transaction) => {

export class vendorModule{  
  @ruleService(rulesProcessor)
   async registerVendor(memberPayload,transaction) {
    const returnObj = {
      success: false,
      message: '',
      data: null,
      };
      //Initialize the entity
      const usersEntity = new UsersEntity();

      // const validation  = await memberRules.validateUser(memberPayload);

      //Insert into Entity
      // const memberRecord = await usersEntity.insert(memberPayload);
      const memberRecord = await usersEntity.insertTransction(memberPayload,transaction);

      returnObj.success = true;
      returnObj.message = 'Member Registered successfully';
      returnObj.data = memberRecord;
      return returnObj;
  

  };
}
