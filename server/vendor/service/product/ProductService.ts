/*************  © FocusResearch Labs 2020 *********************
Function : Membership service
***************************************************************/
  import {ProductEntity} from '../../data-access/ProductEntity';
  import APIError from '../../helpers/APIError';
  import httpStatus from 'http-status';

  export const addProduct = async productPayload => {
    const returnObj = {
      success: false,
      message: '',
      data: null,
      };
  
    try{
      //Initialize the entity
      const productEntity = new ProductEntity();
      //Insert into Entity
      const productRecord = await productEntity.insert(productPayload);
      returnObj.success = true;
      returnObj.message = 'Product Added Successfully';
      returnObj.data = productRecord;
      return returnObj;
    }catch(err){
      throw new APIError(`Error while Adding the Product ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
    }

  };

  export const editProduct = async productPayload => {

    const returnObj = {
      success: false,
      message: '',
      data: null,
      };
    try{
      //Initialize the entity
      const productEntity = new ProductEntity();
      //Insert into Entity
      const productRecord = await productEntity.update(productPayload);
      returnObj.success = true;
      returnObj.message = 'Product updated Successfully';
      returnObj.data = productRecord;
      return returnObj;
    }catch(err){
      throw new APIError(`Error while Updating the Product ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
    }

  };

   export const uploadProductImage = async productPayload => {

    const returnObj = {
      success: false,
      message: '',
      data: null,
      }; 
    try{
      //Initialize the entity
      const productEntity = new ProductEntity();
      //Insert into Entity
      const productRecord = await productEntity.updateImage(productPayload);
      returnObj.success = true;
      returnObj.message = 'Product Image Has Been Updated Successfully';
      returnObj.data = productRecord;
      return returnObj;
    }catch(err){
      throw new APIError(`Error while Updating the Product Image ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
    }

  };

 export const editSingleProduct = async productPayload => {

    const returnObj = {
      success: false,
      message: '',
      data: null,
      };
    try{
      //Initialize the entity
      const productEntity = new ProductEntity();
      //Insert into Entity
      const productRecord = await productEntity.updatesingleproduct(productPayload);
      returnObj.success = true;
      returnObj.message = 'Product updated Successfully';
      returnObj.data = productRecord;
      return returnObj;
    }catch(err){
      throw new APIError(`Error while Updating the Product ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
    }

  };


 export const deleteProduct = async productPayload => {

    const returnObj = {
      success: false,
      message: '',
      data: null,
      };
    try{
      //Initialize the entity
      const productEntity = new ProductEntity();
      //Insert into Entity
      const productRecord = await productEntity.remove(productPayload.params.id);
      returnObj.success = true;
      returnObj.message = 'Product deleted Successfully';
      returnObj.data = productRecord;
      return returnObj;
    }catch(err){
      throw new APIError(`Error while deleting the Product ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
    }

  };