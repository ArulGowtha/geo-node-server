/*************  © FocusResearch Labs 2020 *********************
Function : Service routes
***************************************************************/
import {addProduct ,editProduct ,uploadProductImage, editSingleProduct, deleteProduct } from './ProductService';

const handleInterface = intRequestPayload => {
  // TODO: Make real third party api call
};
 
const productService = Object.freeze({
  addProduct,
  editProduct,
  uploadProductImage,
  editSingleProduct,
  deleteProduct
}); 

export default productService;
//export { findMember, registerMember }