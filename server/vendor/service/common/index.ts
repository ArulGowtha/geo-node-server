/*************  © FocusResearch Labs 2020 *********************
Function : Service routes
***************************************************************/
import {loginUser } from './AuthService';

const handleInterface = intRequestPayload => {
  // TODO: Make real third party api call
};
 
const authService = Object.freeze({
  loginUser
}); 

export default authService;
