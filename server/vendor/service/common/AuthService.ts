/*************  © FocusResearch Labs 2020 *********************
Function : Authentication service
***************************************************************/
  // import MemberEntity from '../../data-access/MemberEntity';
  import {UsersEntity} from '../../data-access/UserEntity';
  import APIError from '../../helpers/APIError';
  import httpStatus from 'http-status';
  import bcrypt from 'bcrypt';
  import jwt from 'jsonwebtoken';
  import config from '../../config/env';




  export const loginUser = async memberPayload => { 
 
    const returnObj = {
      success: false,
      message: '',
      data: null,
      }; 
  
    try{
      const UserEntity = new UsersEntity(); 
      const memberRecord:any = await UserEntity.findUser(memberPayload);
      if(memberRecord != ""){
       var validPassword =  bcrypt.compareSync(memberPayload.body.password,memberRecord[0].authInfo[0].password);
       if(!validPassword){
       throw new APIError(`Password Is Incorrect`, httpStatus.UNAUTHORIZED);
       }else{
       var jwtTokenAuth={ 
          _id:memberRecord[0]._id,
          email:memberRecord[0].email,
        }
        const jwtAccessToken = jwt.sign(jwtTokenAuth, config.default.jwtSecret);
        const returnObj = {
          success: true,
          message: 'Sucessfully Logged In',
          data: {},
        };
        returnObj.data = {
          user: memberRecord[0].vendorsName,
          jwtAccessToken: `JWT ${jwtAccessToken}`,
        };
        return returnObj
       }
      }
      else{
      throw new APIError(`User Not Found`, httpStatus.NOT_FOUND);
      }
     
    }catch(err){
      throw new APIError(`Error while Login The User ${err}`, httpStatus.INTERNAL_SERVER_ERROR);
    }
 
  };


