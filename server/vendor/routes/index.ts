/*************  © FocusResearch Labs 2020 *********************
Function : global routes
***************************************************************/
import express from 'express'; 
import authRoutes from './auth';
import memberRoutes from './member';
import productRoutes from './product';
import inventoryRoutes from './inventory';
import subscriptionRoutes from './subscription';
import passport from 'passport';
import config from '../config/env'; 
import APIError from '../helpers/APIError';
import httpStatus from 'http-status';
 
export const router = express.Router();

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) => res.send('OK'));

router.get('/', (req, res) => res.send('OK'));

// router.use((req, res, next) => {


// var apis = ['/api/member/registerVendor','/api/auth/loginuser']
// var noAuthentication = apis.some(removeAuthentication);
// function removeAuthentication(urls) {
//   return urls == req.originalUrl;
// }

// if(noAuthentication != true){
//         passport.authenticate('jwt', config.passportOptions, (error, authDetail, info) => {
//           if (error) {
//             const err = new APIError(`token not matched ${error}`, httpStatus.UNAUTHORIZED);
//             return next(err);
//           }else if(authDetail){
//             next();
//           }else{
//             const err = new APIError(`token not matched and error msg ${info}`, httpStatus.UNAUTHORIZED);
//             return next(err);         
//           }
//         })(req, res, next);     
// }else{ 
// next();          
// }
// });  


// mount auth routes at /auth
router.use('/auth', authRoutes);

router.use('/member', memberRoutes);

router.use('/product', productRoutes);

router.use('/productinventory', inventoryRoutes);

router.use('/subscription', subscriptionRoutes);

