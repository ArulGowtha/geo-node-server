/*************  © FocusResearch Labs 2020 *********************
Function : Inventory routes
***************************************************************/
import express from 'express'; 
import { inventory, viewproducts, inventoryexcel } from '../controllers/inventory';
import {makeCallback, makeCallbackExcel} from '../express-callback';

const multer = require('multer');  
const mime = require('mime');
const fs = require('fs');
var json2xls = require('json2xls');
const filename = 'productinventory.xlsx';
var exceltojson = require("xlsx-to-json-lc");  




const router = express.Router(); 

var inventorystorage = multer.diskStorage({
    destination: function (req, file, cb) {
      var validExts = ['jpeg', 'jpg', 'png'];
    if(validExts.indexOf(mime.getExtension(file.mimetype))<0){
      return cb(new Error("Wrong format"), null);
    }
      cb(null, './uploads/product-inventory')
    },
    filename: function (req, file, cb) {
    let fileName = `${file.fieldname}-${Date.now()}${'.'+mime.getExtension(file.mimetype)}`;
    file.newName = fileName;
    req.file = file;
    cb(null, fileName)

    } 
  })
 
   var inventoryupload = multer({ storage: inventorystorage});


//*************INVENTORY API - SINGLE API HIT FOR POSTING ALL THE DETAILS ***************//

router.route('/').post(inventoryupload.single('image'),makeCallback(inventory));

//*************INVENTORY API - VIEW PRODUCTS API ***************//

router.route('/viewproducts').post(makeCallback(viewproducts));

//*************INVENTORY API - EXCEL TO JSON CONVERTOR API ***************//


router.route('/exceltojsonproductlist').post(makeCallbackExcel(inventoryexcel));



//***********SAMPLE JSON TO EXCEL CONVERTOR************//
 
router.post('/jsontoexcel',(req, res) =>{  
 
  var xls = json2xls(req.body,{
      fields: ['name','description','keywords','barcode','category','subcategory','aliasName','unit','qty','price','options[0].qty','options[0].price','options[1].qty','options[1].price','image']
  });

  fs.writeFileSync(filename, xls, 'binary', (err) => {
     if (err) {
           console.log("writeFileSync :", err);
      }
 });
  res.send('Successfully the excel has been created');
} 
 

  );

export default router;
