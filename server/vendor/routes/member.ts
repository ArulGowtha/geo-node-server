/*************  © FocusResearch Labs 2020 *********************
Function : member functionality routes
***************************************************************/
import express from 'express'; 
import {findMember, registerVendor} from '../controllers/vendor';
import {makeCallback, makeCallbackExcel} from '../express-callback'; 




const router = express.Router();

//************* VENDOR API - REGISTERATION *******************//
router.route('/registerVendor').post(makeCallback(registerVendor));

//************* VENDOR API - FIND MEMBER EXIST*******************//
router.route('/findMember').post(makeCallback(findMember));

export default router;
