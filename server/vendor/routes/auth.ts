/*************  © FocusResearch Labs 2020 *********************
Function : authentication routes
***************************************************************/
import express from 'express';
import {makeCallback, makeCallbackExcel} from '../express-callback'; 
import {loginUser} from '../controllers/authorization'; 
// import authCtrl from '../service/common/auth'; 
import config from '../config/env';

const router = express.Router();


router.route('/loginuser').post(makeCallback(loginUser));


// router.route('/loginuser').post(authCtrl.loginUser);


export default router;
