/*************  © FocusResearch Labs 2020 *********************
Function : products routes
***************************************************************/
import express from 'express';  
import {addProduct ,editProduct, imageUpload, editSingleProduct, deleteProduct} from '../controllers/products';
import {makeCallback, makeCallbackExcel} from '../express-callback';
const multer = require('multer');  
const mime = require('mime');


const router = express.Router();

var productstorage = multer.diskStorage({
    destination: function (req, file, cb) {
      var validExts = ['jpeg', 'jpg', 'png', 'mpga'];
    if(validExts.indexOf(mime.getExtension(file.mimetype))<0){
      return cb(new Error("Wrong format"), null);
    }
      cb(null, './uploads')
    },
    filename: function (req, file, cb) {
    let fileName = `${file.fieldname}-${Date.now()}${'.'+mime.getExtension(file.mimetype)}`;
    file.newName = fileName;
    req.file = file;
    cb(null, fileName)

    } 
  })
 
   var productuplaod=multer({ storage: productstorage});

//*************ADD AND EDIT USING SINGLE API***************//

router.route('/').post(productuplaod.fields([{ name: 'productimage', maxCount: 1 }, { name: 'adsimage', maxCount: 1 }])
,makeCallback(addProduct)); 

router.route('/editproduct').post(productuplaod.fields([{ name: 'productimage', maxCount: 1 }, { name: 'adsimage', maxCount: 1 }]),makeCallback(editProduct));


//*************Product edit Api***************************//

router.route('/imageupload').post(productuplaod.single('productimage'),makeCallback(imageUpload));

router.route('/:id').put(makeCallback(editSingleProduct));
router.route('/:id').delete(makeCallback(deleteProduct));


export default router;
