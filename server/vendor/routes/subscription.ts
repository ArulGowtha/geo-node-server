/*************  © FocusResearch Labs 2020 *********************
Function : subscription routes
***************************************************************/
import express from 'express'; 
// import {subscription} from '../controllers/subscription-controller';
import {makeCallback, makeCallbackExcel} from '../express-callback';


const router = express.Router(); 


//*************SUBSCRIPTION API***************//

// router.route('/').post(makeCallback(subscription));


export default router;
