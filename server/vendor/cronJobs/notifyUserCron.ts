// /*************  © FocusResearch Labs 2020 *********************
// Function : Notification cron job.
// ***************************************************************/
// import moment from 'moment';
// import MemberSchema from '../models/User';
// import { sendSms } from '../service/util/smsApi';

// const findUnsubscribedProducts = (startDate, endDate) => {
//   MemberSchema.findAsync({ bookingTime: { $lte: startDate, $gte: endDate }, MemberStatus: 'unsubscribed' })
//     .then((subData) => {
//       if (subData.length > 0) {
//         subData.forEach((item) => {
//           const smsText = '';
//           sendSms(item.riderId, smsText, (err, data) => {
//             if (err) {
//               console.log(err);
//             } else {
//               console.log(data);
//             }
//           });
//         });
//       } else {
//         console.log('No unsubscribed products found ', startDate);
//       }
//     })
//     .catch((error) => {
//       console.log('Server error finding unsubscribed products', error);
//     });
// };

// export const notifyUserUnSubscribedProducts = () => {
//   const currentDate = new Date().toISOString();
//   const tomorrowEndDate = new Date(moment(currentDate).add(1, 'days'));
//   tomorrowEndDate.setHours(23);
//   tomorrowEndDate.setMinutes(59);
//   tomorrowEndDate.setSeconds(59);
//   const tomorrowStartDate = new Date(moment(currentDate).add(1, 'days'));
//   tomorrowStartDate.setHours(0);
//   tomorrowStartDate.setMinutes(0);
//   tomorrowStartDate.setSeconds(0);
//   findUnsubscribedProducts(tomorrowStartDate, tomorrowEndDate);
// };
