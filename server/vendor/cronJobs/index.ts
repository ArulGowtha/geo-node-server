// /*************  © FocusResearch Labs 2020 *********************
// Function : System cron jobs
// ***************************************************************/
// import cron from 'node-cron';
// import * as notifyUser from './notifyUserCron';
// //import sendEmail from '../service/emailApi';

// // notify rider and driver on scheduled trips before an hour of scheduled time
// export const notifyUserUnSubscribedProducts = (/* req, res */) => {
//   // Cron runs every minute to check scheduled accepted requests after an hour and notify .
//   // cron.schedule('*/1 * * * *', () => {
//   //   console.log('checking Subscription renewals');
//   //   //notifyUser.notifyUserUnSubscribedProducts();
//   // });
// };

// // not being used
// export const emailUserWeekly = (/* req, res */) => {
//   cron.schedule('* * * * Monday', () => {
//     //sendEmail(); // Cron runs every monday to email vendor weekly stats.
//   });
// };
